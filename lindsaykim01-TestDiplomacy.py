#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = "A Madrid Support B\nB Austin Hold\nC Houston Move Madrid"
        w = diplomacy_solve(r)
        self.assertEqual(w, "A Madrid\nB Austin\nC [dead]\n")

    def test_solve_2(self):
        r = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A"
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_3(self):
        r = "A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF Fortworth Support E"
        w = diplomacy_solve(r)
        self.assertEqual(w, "A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF Fortworth\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()
""" #pragma: no cover"""