from unittest import main, TestCase
from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_eval, \
    diplomacy_final_stance, armyObject, ArmyList, diplomacy_check_conflict
from io import StringIO

class TestDiplomacy(TestCase):
    def test_eval1(self):
        tracker = ArmyList()
        tracker.add_army(armyObject("A", "Madrid"))
        v = diplomacy_eval(tracker)
        self.assertEqual(str(v), "A Madrid\n")

    def test_eval2(self):
        tracker = ArmyList()
        tracker.add_army(armyObject("A", "Madrid", 1))
        tracker.add_army(armyObject("B", "Austin"))
        tracker.add_army(armyObject("C", "Madrid", 1))
        tracker.add_army(armyObject("D", "NewYork"))
        tracker.add_army(armyObject("E", "Kyiv", 1))
        tracker.add_army(armyObject("F", "Kyiv"))
        tracker.add_army(armyObject("G", "Madrid", 0))
        v = diplomacy_eval(tracker)
        self.assertEqual(str(v), "A [dead]\nB Austin\nC [dead]\nD NewYork\nE Kyiv\nF [dead]\nG [dead]\n")

    def test_eval3(self):
        tracker = ArmyList()
        tracker.add_army(armyObject("A", "Madrid"))
        tracker.add_army(armyObject("B", "Madrid", 1))
        tracker.add_army(armyObject("C", "Madrid", 1))
        v = diplomacy_eval(tracker)
        self.assertEqual(str(v), "A [dead]\nB [dead]\nC [dead]\n")
    
    def test_check_conflict1(self):
        cg = {"Madrid":[("A",0)]}
        tracker = ArmyList()
        tracker.add_army(armyObject("A", "Madrid"))
        v = diplomacy_check_conflict(cg, tracker)
        self.assertEqual(v, {"Madrid":[("A", 0)]})

    def test_check_conflict2(self):
        cg = {"Madrid":[("A", 0), ("B", 2), ("C",1)]}
        tracker = ArmyList()
        tracker.add_army(armyObject("A", "Madrid"))
        tracker.add_army(armyObject("B", "Madrid", 2))
        tracker.add_army(armyObject("C", "Madrid", 1))
        v = diplomacy_check_conflict(cg, tracker)
        self.assertEqual(v, {"Madrid":[("A", 0), ("C", 1), ("B", 2)]})

    def test_check_conflict3(self):
        cg = {"Madrid":[("A", 0), ("F", 2), ("C",1)], "NewYork":[("D", 0), ("B", 2), ("E",1)]}
        tracker = ArmyList()
        tracker.add_army(armyObject("A", "Madrid"))
        tracker.add_army(armyObject("F", "Madrid", 2))
        tracker.add_army(armyObject("C", "Madrid", 1))
        tracker.add_army(armyObject("D", "NewYork"))
        tracker.add_army(armyObject("B", "NewYork", 2))
        tracker.add_army(armyObject("E", "NewYork", 1))
        v = diplomacy_check_conflict(cg, tracker)
        self.assertEqual(v, {"Madrid":[("A", 0), ("C", 1), ("F", 2)], "NewYork":[("D", 0), ("E", 1), ("B",2)]})

    def test_diplomacy_final_stance1(self):
        tracker = ArmyList()
        diplomacy_final_stance(tracker, "A", "Madrid", "Hold")
        self.assertEqual(str(tracker), "A Madrid\n")

    def test_diplomacy_final_stance2(self):
        tracker = ArmyList()
        diplomacy_final_stance(tracker, "A", "Austin", "Support", "B")
        diplomacy_final_stance(tracker, "B", "Boston", "Support", "C")
        diplomacy_final_stance(tracker, "C", "Charlotte", "Hold")
        diplomacy_final_stance(tracker, "D", "Dallas", "Move", "Boston")
        diplomacy_final_stance(tracker, "E", "Edmonton", "Move", "Charlotte")
        diplomacy_final_stance(tracker, "F", "FortWorth", "Support", "E")
        self.assertEqual(str(tracker), "A Austin\nB Boston\nC Charlotte\nD Boston\nE Charlotte\nF FortWorth\n")

    def test_diplomacy_final_stance3(self):
        tracker = ArmyList()
        diplomacy_final_stance(tracker, "A", "Austin", "Move", "Boston")
        diplomacy_final_stance(tracker, "B", "Boston", "Move", "Austin")
        self.assertEqual(str(tracker), "A Boston\nB Austin\n")

    def test_diplomacy_final_stance4(self):
        tracker = ArmyList()
        diplomacy_final_stance(tracker, "A", "Austin", "Move", "Boston")
        diplomacy_final_stance(tracker, "B", "Boston", "Move", "Austin")
        diplomacy_final_stance(tracker, "C", "Boston", "Support", "B")
        diplomacy_final_stance(tracker, "B", "Austin", "Move", "Madrid")
        diplomacy_final_stance(tracker, "D", "Madrid", "Hold")
        self.assertEqual(str(tracker), "A Boston\nB Madrid\nC Boston\nD Madrid\n")

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Paris Move Madrid\nC NewYork Move Madrid\nD Austin Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Madrid\nD Austin\n")

    def test_solve2(self):
        r = StringIO("A Austin Move Boston\nB Boston Move Austin\nC Boston Support B\nB Austin Move Madrid\nD Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve3(self):
        r = StringIO("A Austin Hold\nB Boston Hold\nC Houston Hold\nD NewYork Move Houston")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Boston\nC [dead]\nD [dead]\n")

    def test_read1(self):
        v = diplomacy_read("A Madrid Hold")
        self.assertEqual(v, ["A", "Madrid", "Hold"])

    def test_read2(self):
        v = diplomacy_read("")
        self.assertEqual(v, [])

    def test_read3(self):
        v = diplomacy_read("A Madrid Move Houston")
        self.assertEqual(v, ["A", "Madrid", "Move", "Houston"])

if __name__ == "__main__":
    main()


#ArmyList is a list of armyobjects.
#How will I access these objects?
# iterate thru it??
# why cant i just use a dictionary for easy O(1) lookup?