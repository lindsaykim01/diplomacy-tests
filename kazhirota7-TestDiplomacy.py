#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/kazhirota7-TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# Testdiplomacy
# -----------


class Testdiplomacy (TestCase):
    # ----
    # read
    # ----

    # def test_read(self):
    #     s = "1 10\n"
    #     i, j = diplomacy_read(s)
    #     self.assertEqual(i,  1)
    #     self.assertEqual(j, 10)

    # def test_read_2(self):
    #     s = "100 200\n"
    #     i, j = diplomacy_read(s)
    #     self.assertEqual(i,  100)
    #     self.assertEqual(j, 200)

    # def test_read_3(self):
    #     s = "900 1000\n"
    #     i, j = diplomacy_read(s)
    #     self.assertFalse(i == 1)
    #     self.assertFalse(j == 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        result = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"])
        self.assertEqual(result, ["A [dead]", "B Madrid", "C London"])

    def test_eval_2(self):
        result = diplomacy_eval(["A Madrid Hold"])
        self.assertEqual(result, ["A Madrid"])

    def test_eval_3(self):
        result = diplomacy_eval(["A Madrid Move Barcelona", "B Barcelona Move Madrid"])
        self.assertFalse(result == ["A [dead]", "B [dead]"])

    
    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, ["A Madrid"])
        self.assertEqual(w.getvalue(), "A Madrid")
    
    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid", "C London"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London")

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid"])
        self.assertFalse(w.getvalue() == "A [dead]\nB Madrid\n")
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]")

    def test_solve3(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid")

    def test_solve4(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertFalse(
            w.getvalue() == "A [dead]")

    def test_solve5(self):
        r= StringIO("A Madrid Move Barcelona\nB Barcelona Hold\nC London Support B")
        w= StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC London")
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch Testdiplomacy.py >  Testdiplomacy.out 2>&1


$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> Testdiplomacy.out



$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
diplomacy.py          12      0      2      0   100%
Testdiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
